## Bulpay

#### Bulgarian Paying system
REST app using Maven, Tomcat & Servlets

1. /user
    - GET - get all users
    - POST - format : {"username":"X","password":"X","firstName":"X","lastName":"X","money":X}
    - PUT - format : {"id":X,"username":"X","password":"X","firstName":"X","lastName":"X","money":X}
    - DELETE - id=X as param

2. /card 
	- GET - get all cards
	- POST - format : {"owner":{},"status":"X","issued":"YYYY-MM-DDTHH:MM:SS.sssssss","expiration":"YYYY-MM-DDTHH:MM:SS.sssssss"}
	- PUT - format : {"id":X,"owner":{},"status":"X","issued":"YYYY-MM-DDTHH:MM:SS.sssssss","expiration":"YYYY-MM-DDTHH:MM:SS.sssssss"}
	- DELETE - id=X as param
	
3. /transaction 
    - GET - get all transactions
    - POST - format : {"sender":{},"receiver":{},"money":X,"created":"YYYY-MM-DDTHH:MM:SS.sssssss"}
    - PUT - format : {"id":X,"sender":{},"receiver":{},"money":X,"created":"YYYY-MM-DDTHH:MM:SS.sssssss"}
    - DELETE - id=X as param