package com.isoft.bulpay.domain.entities;

import com.isoft.bulpay.domain.entities.enums.Status;

import java.time.LocalDateTime;

public class Card
{
    private Integer id;
    private User owner;
    private Status status;
    private LocalDateTime issued;
    private LocalDateTime expiration;

    public Card() {}

    public Card(Integer id, User owner, Status status, LocalDateTime issued, LocalDateTime expiration)
    {
        this.id = id;
        this.owner = owner;
        this.status = status;
        this.issued = issued;
        this.expiration = expiration;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public LocalDateTime getIssued()
    {
        return issued;
    }

    public void setIssued(LocalDateTime issued)
    {
        this.issued = issued;
    }

    public LocalDateTime getExpiration()
    {
        return expiration;
    }

    public void setExpiration(LocalDateTime expiration)
    {
        this.expiration = expiration;
    }

    public User getOwner()
    {
        return owner;
    }

    public void setOwner(User owner)
    {
        this.owner = owner;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }
}
