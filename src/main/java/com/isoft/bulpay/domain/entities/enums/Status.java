package com.isoft.bulpay.domain.entities.enums;

public enum Status
{
    IN_PROGRESS,
    EXPIRED,
    NORMAL,
    VIP
}
