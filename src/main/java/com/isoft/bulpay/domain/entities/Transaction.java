package com.isoft.bulpay.domain.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transaction
{
    private Integer id;
    private User sender;
    private User receiver;
    private BigDecimal money;
    private LocalDateTime created;

    public Transaction() {}

    public Transaction(Integer id, User sender, User receiver, BigDecimal money, LocalDateTime created) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.money = money;
        this.created = created;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public User getSender()
    {
        return sender;
    }

    public void setSender(User sender)
    {
        this.sender = sender;
    }

    public User getReceiver()
    {
        return receiver;
    }

    public void setReceiver(User receiver)
    {
        this.receiver = receiver;
    }

    public BigDecimal getMoney()
    {
        return money;
    }

    public void setMoney(BigDecimal money)
    {
        this.money = money;
    }

    public LocalDateTime getCreated()
    {
        return created;
    }

    public void setCreated(LocalDateTime created)
    {
        this.created = created;
    }
}