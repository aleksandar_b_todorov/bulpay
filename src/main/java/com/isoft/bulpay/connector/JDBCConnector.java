package com.isoft.bulpay.connector;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.Connection;

public class JDBCConnector
{
    private static Properties props = new Properties();
    private static BasicDataSource connectionPool = new BasicDataSource();
    private static final Logger LOGGER = LogManager.getLogger(JDBCConnector.class.getName());

    private JDBCConnector(){}

    static
    {
        try (FileInputStream in = new FileInputStream(".\\src\\main\\resources\\database.properties"))
        {
            props.load(in);
            connectionPool.setDriverClassName(props.getProperty("db.driver.class"));
            connectionPool.setUrl(props.getProperty("db.conn.url"));
            connectionPool.setUsername(props.getProperty("db.username"));
            connectionPool.setPassword(props.getProperty("db.password"));
            connectionPool.setMinIdle(5);
            connectionPool.setMaxIdle(10);
            connectionPool.setMaxOpenPreparedStatements(100);
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        }
    }

    public static Connection getConnection() throws SQLException
    {
        return connectionPool.getConnection();
    }


}
