package com.isoft.bulpay.repository.impl;

import com.isoft.bulpay.connector.JDBCConnector;
import com.isoft.bulpay.domain.entities.Transaction;
import com.isoft.bulpay.domain.entities.User;
import com.isoft.bulpay.repository.api.CrudRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TransactionRepository implements CrudRepository
{
    private static final Logger LOGGER = LogManager.getLogger(TransactionRepository.class.getName());
    private final UserRepository userRepository = new UserRepository();

    @Override
    public List<Transaction> getAll()
    {
        String SQL = "SELECT * FROM transactions";
        List<Transaction> transactions = new ArrayList<>();
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL);
             ResultSet rs = statement.executeQuery())
        {
            while (rs.next())
            {
                Transaction transaction = initializeTransaction(rs);
                transactions.add(transaction);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return new ArrayList<>();
        }
        return transactions;
    }

    @Override
    public Transaction getById(Integer id)
    {
        String SQL = "SELECT * FROM transactions WHERE id=?";
        Transaction transaction = null;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
            {
                transaction = initializeTransaction(rs);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return null;
        }
        return transaction;
    }

    @Override
    public boolean create(Object entity)
    {
        String SQL = "INSERT INTO transactions(sender_id, receiver_id, money, created) "
                + "VALUES(?,?,?,?)";
        Transaction transaction = (Transaction) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setTransactionParams(transaction, statement);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Integer id, Object entity)
    {
        String SQL = "UPDATE transactions SET sender_id=?, receiver_id=?, money=?, created=? WHERE id=?";
        Transaction transactionToUpdate = (Transaction) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setTransactionParams(transactionToUpdate, statement);
            statement.setInt(5, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteById(Integer id)
    {
        String SQL = "DELETE FROM transactions WHERE id=?";

        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    private Transaction initializeTransaction(ResultSet rs) throws SQLException
    {
        Integer transactionId = rs.getInt("id");
        User sender = userRepository.getById(rs.getInt("sender_id"));
        User receiver = userRepository.getById(rs.getInt("receiver_id"));
        BigDecimal money = rs.getBigDecimal("money");
        LocalDateTime created = rs.getTimestamp("created").toLocalDateTime();

        return new Transaction(transactionId, sender, receiver, money, created);
    }

    private void setTransactionParams(Transaction transaction, PreparedStatement statement) throws SQLException
    {
        statement.setInt(1, transaction.getSender().getId());
        statement.setInt(2, transaction.getReceiver().getId());
        statement.setBigDecimal(3, transaction.getMoney());
        statement.setTimestamp(4, Timestamp.valueOf(transaction.getCreated()));
    }
}
