package com.isoft.bulpay.repository.impl;

import com.isoft.bulpay.connector.JDBCConnector;
import com.isoft.bulpay.domain.entities.User;
import com.isoft.bulpay.repository.api.CrudRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository implements CrudRepository
{
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class.getName());

    @Override
    public List<User> getAll()
    {
        String SQL = "SELECT * FROM users";
        List<User> users = new ArrayList<>();
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL);
             ResultSet rs = statement.executeQuery())
        {
            while (rs.next())
            {
                User user = initializeUser(rs);
                users.add(user);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return new ArrayList<>();
        }
        return users;
    }

    @Override
    public User getById(Integer id)
    {
        String SQL = "SELECT * FROM users WHERE id=?";
        User user = null;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
            {
                user = initializeUser(rs);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return null;
        }
        return user;
    }

    @Override
    public boolean create(Object entity)
    {
        String SQL = "INSERT INTO users(username,password,first_name,last_name,money) "
                + "VALUES(?,?,?,?,?)";
        User user = (User) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setUserParams(user, statement);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Integer id, Object entity)
    {
        String SQL = "UPDATE users SET username=?, password=?, first_name=?, last_name=?, money=? WHERE id=?";
        User userToUpdate = (User) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setUserParams(userToUpdate, statement);
            statement.setInt(6, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteById(Integer id)
    {
        String SQL = "DELETE FROM users WHERE id=?";

        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    private User initializeUser(ResultSet rs) throws SQLException
    {
        Integer userId = rs.getInt("id");
        String userUsername = rs.getString("username");
        String userPassword = rs.getString("password");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        BigDecimal money = rs.getBigDecimal("money");

        return new User(userId, userUsername, userPassword, firstName, lastName, money);
    }

    private void setUserParams(User user, PreparedStatement statement) throws SQLException
    {
        statement.setString(1, user.getUsername());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getFirstName());
        statement.setString(4, user.getLastName());
        statement.setBigDecimal(5, user.getMoney());
    }
}
