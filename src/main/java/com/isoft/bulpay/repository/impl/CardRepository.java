package com.isoft.bulpay.repository.impl;

import com.isoft.bulpay.connector.JDBCConnector;
import com.isoft.bulpay.domain.entities.Card;
import com.isoft.bulpay.domain.entities.User;
import com.isoft.bulpay.domain.entities.enums.Status;
import com.isoft.bulpay.repository.api.CrudRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CardRepository implements CrudRepository
{
    private static final Logger LOGGER = LogManager.getLogger(CardRepository.class.getName());
    private final UserRepository userRepository = new UserRepository();

    @Override
    public List<Card> getAll()
    {
        String SQL = "SELECT * FROM cards";
        List<Card> cards = new ArrayList<>();
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL);
             ResultSet rs = statement.executeQuery())
        {
            while (rs.next())
            {
                Card card = initializeCard(rs);
                cards.add(card);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return new ArrayList<>();
        }
        return cards;
    }

    @Override
    public Card getById(Integer id)
    {
        String SQL = "SELECT * FROM cards WHERE id=?";
        Card card = null;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
            {
                card = initializeCard(rs);
            }
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return null;
        }
        return card;
    }

    @Override
    public boolean create(Object entity)
    {
        String SQL = "INSERT INTO cards(owner, status, issued, expiration) "
                + "VALUES(?,?,?,?)";
        Card card = (Card) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setCardParams(card, statement);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Integer id, Object entity)
    {
        String SQL = "UPDATE cards SET owner=?, status=?, issued=?, expiration=? WHERE id=?";
        Card cardToUpdate = (Card) entity;
        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            setCardParams(cardToUpdate, statement);
            statement.setInt(5, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteById(Integer id)
    {
        String SQL = "DELETE FROM cards WHERE id=?";

        try (Connection connection = JDBCConnector.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException sqle)
        {
            LOGGER.error(sqle);
            return false;
        }
        return true;
    }

    private Card initializeCard(ResultSet rs) throws SQLException
    {
        Integer cardId = rs.getInt("id");
        User user = userRepository.getById(rs.getInt("owner"));
        Status status = Status.valueOf(rs.getString("status"));
        LocalDateTime issued = rs.getTimestamp("issued").toLocalDateTime();
        LocalDateTime expiration = rs.getTimestamp("expiration").toLocalDateTime();

        return new Card(cardId, user, status, issued, expiration);
    }

    private void setCardParams(Card card, PreparedStatement statement) throws SQLException
    {
        statement.setInt(1, card.getOwner().getId());
        statement.setString(2, card.getStatus().name());
        statement.setTimestamp(3, Timestamp.valueOf(card.getIssued()));
        statement.setTimestamp(4, Timestamp.valueOf(card.getExpiration()));
    }
}
