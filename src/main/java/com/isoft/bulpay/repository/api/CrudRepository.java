package com.isoft.bulpay.repository.api;

import java.io.IOException;
import java.util.List;

/**
 * Generic Crud operations
 * @param <T>
 * @author Aleksandar Todorov
 */
public interface CrudRepository<T>
{
    List<T> getAll() throws IOException;

    T getById(Integer id);

    boolean create(T entity);

    boolean update(Integer id, T entity);

    boolean deleteById(Integer id);
}
