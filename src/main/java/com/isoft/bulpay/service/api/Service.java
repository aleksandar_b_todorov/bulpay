package com.isoft.bulpay.service.api;

import java.io.IOException;

/**
 * Generic Crud operations
 * @param <T>
 * @author Aleksandar Todorov
 */
public interface Service<T>
{
    String getAll() throws IOException;

    boolean create(T entity);

    boolean deleteById(String id);

    boolean put(String json);
}
