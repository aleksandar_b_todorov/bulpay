package com.isoft.bulpay.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.isoft.bulpay.domain.entities.Transaction;
import com.isoft.bulpay.repository.impl.TransactionRepository;
import com.isoft.bulpay.service.api.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class TransactionService implements Service
{
    private final TransactionRepository transactionRepository = new TransactionRepository();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(TransactionService.class.getName());

    @Override
    public String getAll() throws IOException
    {
        List<Transaction> transactions = transactionRepository.getAll();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        objectMapper.writeValue(out, transactions);

        final byte[] data = out.toByteArray();
        return new String(data);
    }

    @Override
    public boolean create(Object entity)
    {
        Transaction transaction = (Transaction) entity;

        if (transaction == null || transaction.getSender() == null || transaction.getReceiver() == null || transaction.getMoney() == null || transaction.getCreated() == null)
        {
            return false;
        }

        // sent money must be positive , and sender must have more or equels amount of the sum one want to send.
        if (transaction.getSender().getMoney().compareTo(BigDecimal.ZERO) > 0 ||
                transaction.getSender().getMoney().compareTo(transaction.getMoney()) >= 0)
        {
            return false;
        }

        transactionRepository.create(transaction);
        return true;
    }

    @Override
    public boolean deleteById(String id)
    {
        if (id == null) return false;

        try
        {
            transactionRepository.deleteById(Integer.parseInt(id));
            return true;
        } catch (NumberFormatException nfe)
        {
            LOGGER.error(nfe);
            return false;
        }
    }

    @Override
    public boolean put(String json)
    {
        Transaction transactionToUpdate;
        try
        {
            objectMapper.registerModule(new JavaTimeModule());
            transactionToUpdate = objectMapper.readValue(json, Transaction.class);
        } catch (IOException e)
        {
            LOGGER.error(e);
            return false;
        }
        if (transactionToUpdate == null) return false;

        Transaction oldTransaction = transactionRepository.getById(transactionToUpdate.getId());

        if (oldTransaction == null) return false;

        transactionRepository.update(oldTransaction.getId(), transactionToUpdate);
        return true;
    }
}
