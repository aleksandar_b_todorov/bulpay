package com.isoft.bulpay.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.bulpay.domain.entities.User;
import com.isoft.bulpay.repository.impl.UserRepository;
import com.isoft.bulpay.service.api.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class UserService implements Service
{
   private final UserRepository userRepository = new UserRepository();
   private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(UserService.class.getName());

    @Override
    public String getAll() throws IOException
    {
        List<User> users = userRepository.getAll();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        objectMapper.writeValue(out, users);

        final byte[] data = out.toByteArray();
        return new String(data);
    }

    @Override
    public boolean create(Object entity)
    {
        User user = (User) entity;

        if (user == null || user.getFirstName() == null || user.getLastName() == null || user.getUsername() == null || user.getPassword() == null)
        {
            return false;
        }
        userRepository.create(user);
        return true;
    }

    @Override
    public boolean deleteById(String id)
    {
        if (id == null) return false;

        try
        {
            userRepository.deleteById(Integer.parseInt(id));
            return true;
        } catch (NumberFormatException nfe)
        {
            LOGGER.error(nfe);
            return false;
        }
    }

    @Override
    public boolean put(String json)
    {
        User userToUpdate;
        try
        {
            userToUpdate = objectMapper.readValue(json, User.class);
        } catch (IOException e)
        {
            LOGGER.error(e);
            return false;
        }
        if (userToUpdate == null) return false;

        User oldUser = userRepository.getById(userToUpdate.getId());

        if(oldUser == null) return false;

        userRepository.update(oldUser.getId(), userToUpdate);
        return true;
    }
}
