package com.isoft.bulpay.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.isoft.bulpay.domain.entities.Card;
import com.isoft.bulpay.repository.impl.CardRepository;
import com.isoft.bulpay.service.api.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class CardService implements Service
{
    private final CardRepository cardRepository = new CardRepository();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(CardService.class.getName());

    @Override
    public String getAll() throws IOException
    {
        List<Card> cards = cardRepository.getAll();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        objectMapper.writeValue(out, cards);

        final byte[] data = out.toByteArray();
        return new String(data);
    }

    @Override
    public boolean create(Object entity)
    {
        Card card = (Card) entity;

        if (card == null || card.getOwner() == null || card.getStatus() == null || card.getIssued() == null || card.getExpiration() == null)
        {
            return false;
        }
        cardRepository.create(card);
        return true;
    }

    @Override
    public boolean deleteById(String id)
    {
        if (id == null) return false;

        try
        {
            cardRepository.deleteById(Integer.parseInt(id));
            return true;
        } catch (NumberFormatException nfe)
        {
            LOGGER.error(nfe);
            return false;
        }
    }

    @Override
    public boolean put(String json)
    {
        Card cardToUpdate;
        try
        {
            objectMapper.registerModule(new JavaTimeModule());
            cardToUpdate = objectMapper.readValue(json, Card.class);
        } catch (IOException e)
        {
            LOGGER.error(e);
            return false;
        }
        if (cardToUpdate == null) return false;

        Card oldCard = cardRepository.getById(cardToUpdate.getId());

        if (oldCard == null) return false;

        cardRepository.update(oldCard.getId(), cardToUpdate);
        return true;
    }
}
