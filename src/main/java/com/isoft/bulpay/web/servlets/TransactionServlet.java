package com.isoft.bulpay.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.isoft.bulpay.domain.entities.Transaction;
import com.isoft.bulpay.service.impl.TransactionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

/**
 * RESTful Web Service implements GET POST DELETE PUT
 * @author Aleksandar Todorov
 */
@WebServlet("/transaction")
public class TransactionServlet extends HttpServlet
{
    private final TransactionService transactionService = new TransactionService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(TransactionServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            PrintWriter out = resp.getWriter();
            resp.setContentType("application/json");
            out.print(transactionService.getAll());
            out.flush();
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            objectMapper.registerModule(new JavaTimeModule());
            transactionService.create(objectMapper.readValue(req.getInputStream(), Transaction.class));
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        } catch (ClassCastException cce)
        {
            LOGGER.error(cce);
        } catch (Exception e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        transactionService.deleteById(req.getParameter("id"));
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            transactionService.put(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        }
    }
}
