package com.isoft.bulpay.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.isoft.bulpay.domain.entities.Card;
import com.isoft.bulpay.service.impl.CardService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

/**
 * RESTful Web Service implements GET POST DELETE PUT
 * @author Aleksandar Todorov
 */
@WebServlet("/card")
public class CardServlet extends HttpServlet
{
    private final CardService cardService = new CardService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(CardServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            PrintWriter out = resp.getWriter();
            resp.setContentType("application/json");
            out.print(cardService.getAll());
            out.flush();
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            objectMapper.registerModule(new JavaTimeModule());
            cardService.create(objectMapper.readValue(req.getInputStream(), Card.class));
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        } catch (ClassCastException cce)
        {
            LOGGER.error(cce);
        } catch (Exception e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        cardService.deleteById(req.getParameter("id"));
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            cardService.put(req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
        } catch (IOException ioe)
        {
            LOGGER.error(ioe);
        }
    }
}
